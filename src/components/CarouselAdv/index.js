import React from 'react';
import Swiper from 'swiper';
import './style.scss';

import Heading from '@c/Text/Heading';
import TextElement from '@c/Text/TextElement';

class CarouselAdv extends React.Component {
  constructor(props) {
    super(props);
    this.swiper = React.createRef();
  }

  componentDidMount() {
    new Swiper(this.swiper.current, {
      loop: false,
      spaceBetween: 50,
    });
  }

  render() {
    const { items } = this.props;

    return (
      <div className="r-carousel-adv">
        <div
          className="swiper-container r-carousel-adv__swiper o-padding-horizontal-container"
          ref={this.swiper}
        >
          <div className="swiper-wrapper">
            {
              items.map((item, index) =>
                <div
                  key={index}
                  className="swiper-slide r-carousel-adv__slide slide"
                >
                  <div
                    className="slide__image"
                    style={{ backgroundImage: `url(${item.image.url})` }}
                  />
                  <a
                    className="slide__content"
                    href={item.url}
                  >
                    <Heading
                      text={item.title}
                      pattern="h2"
                      tag="p"
                      indent="2"
                    />
                    <TextElement
                      text={item.description}
                      size="medium"
                      indent="none"
                    />
                  </a>
                </div>
              )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default CarouselAdv;
