import PropTypes from 'prop-types';
import classNames from 'classnames';

import { safelySetInnerHTML } from '@/helpers/safeHtml';

export default function TextElement(props) {
  const {
    className,
    text,
    tag = 'p',
    size = 'base',
    isHtml = false,
    color = 'white',
    indent,
    alignment,
    transform,
    ...otherAttrs
  } = props;
  const Tag = tag;

  return (
    <Tag
      className={classNames(
        className,
        'u-text',
        {
          [`u-text--${size}`]: size,
          [`u-color-${color}`]: color,
          [`u-adaptive-margin-bottom-${indent}`]: indent,
          [`u-align-${alignment}`]: alignment,
          [`u-${transform}`]: transform,
        }
      )}
      { ...otherAttrs }
      dangerouslySetInnerHTML={isHtml ? safelySetInnerHTML(text) : null}
    >
        {isHtml ? null : text}
    </Tag>
  )
}

TextElement.propTypes = {
  tag: PropTypes.oneOf(['p', 'div', 'span']),
  size: PropTypes.oneOf(['large', 'big', 'medium', 'base', 'small']),

  color: PropTypes.oneOf(['white', 'grey-10', 'indigo']),
  indent: PropTypes.string,
  alignment: PropTypes.oneOf(['left', 'center', 'right']),
  transform: PropTypes.oneOf(['capitalize', 'uppercase']),
};
