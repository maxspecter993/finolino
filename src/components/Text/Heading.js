import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Heading(props) {
  const {
    className,
    text,
    depth = '6',
    tag,
    pattern,
    color = 'white',
    indent,
    alignment,
    transform,
    ...otherAttrs
  } = props;
  const Tag = tag || `h${depth}`;

  return (
    <Tag
      className={classNames(
        className,
        'u-heading',
        {
          [`u-heading--${pattern}`]: pattern,
          [`u-color-${color}`]: color,
          [`u-adaptive-margin-bottom-${indent}`]: indent,
          [`u-align-${alignment}`]: alignment,
          [`u-${transform}`]: transform,
        }
      )}
      { ...otherAttrs }
    >
      {text}
    </Tag>
  )
}

Heading.propTypes = {
  depth: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
  tag: PropTypes.oneOf(['p', 'div']),
  pattern: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']),

  color: PropTypes.oneOf(['white', 'grey-10', 'indigo']),
  indent: PropTypes.string,
  alignment: PropTypes.oneOf(['left', 'center', 'right']),
  transform: PropTypes.oneOf(['capitalize', 'uppercase']),
};
