import './style.scss';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import Btn from '@c/Buttons/Btn';
import Heading from '@c/Text/Heading';
import TextElement from '@c/Text/TextElement';

export default function CarouselHeroSlide(props) {
  const {
    title,
    description,
    btn,
    textColor,
    image,
    imagePosition = 'right',
    className
  } = props;

  const slideContent = (
    <div
      className={ classNames(
        'r-hero-slide__content',
        { 'u-padding-left-8@device': imagePosition === 'left' }
      ) }
      key="content"
    >
      {
        title
        &&
        <Heading
          data-swiper-parallax="-400"
          className="r-hero-slide__title"
          text={title}
          depth={1}
          pattern="h1"
          indent="4"
          color={textColor}
        />
      }
      {
        description
        &&
        <TextElement
          data-swiper-parallax="-200"
          size="medium"
          text={description}
          indent="4"
          color={textColor}
        />
      }
      {
        (btn && btn.text)
        &&
        <Btn
          url={btn.url}
          text={btn.text}
          tag='a'
          roundingType='top-right'
          size="default"
          color={textColor}
        />
      }
    </div>
  )

  const slideMedia = (
    <div
      className="r-hero-slide__media"
      key="media"
    >
      {
        (image && image.url)
        &&
        <img
          data-swiper-parallax="-600"
          className="r-hero-slide__img"
          src={image.url}
          alt={image.alt}
        />
      }
    </div>
  )

  const slideInner = (imagePosition === 'right') ?
  [ slideContent, slideMedia ] :
  [ slideMedia, slideContent ];

  return (
    <div
      className={classNames(
        className,
        'r-hero-slide o-padding-horizontal-container',
      )}
    >
      { slideInner }
    </div>
  )
}


CarouselHeroSlide.propTypes = {
  imagePosition: PropTypes.oneOf(['left', 'right']),
}
