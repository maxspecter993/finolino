import React from 'react';
import './style.scss';

import Heading from '@c/Text/Heading';

class HalfSelector extends React.Component {

  render() {
    const { items } = this.props;

    return (
      <div className="r-half-selector o-adjoin-container">
        {
          items.map((item, index) =>
            <a
              className="r-half-selector__cta cta"
              href={ item.url }
              key={ index }
            >
              <div
                className="cta__image"
                style={{ backgroundImage: `url(${item.imageUrl})` }}
              />
              <div className="cta__title">
                <Heading
                  tag="p"
                  pattern="h1"
                  text={item.title}
                  indent="none"
                  alignment="center"
                />
              </div>
            </a>
          )
        }
      </div>
    );
  }
};

export default HalfSelector;
