import { Component } from 'react';
import './style.scss';

export default class GridMain extends Component {
  render() {
    return (
      <div className="r-grid-main">
        {this.props.slot}
      </div>
    );
  }
};
