import { Routes, Route } from "react-router-dom";
import Header from '@c/Header';

import AboutUsPage from '@c/Pages/AboutUsPage';
import CatalogPage from '@c/Pages/CatalogPage';
import ContactUsPage from '@c/Pages/ContactUsPage';
import FAQsPage from '@c/Pages/FAQsPage';
import HomePage from '@c/Pages/HomePage';
import NotFoundPage from '@c/Pages/NotFoundPage';

function App() {
  return (
    <div className="app">
      <div className="app__bg">
        <div className="app__bg__color" />
        <div className="app__bg__img" />
      </div>
      <Header />
      <Routes>
        <Route
          path="/"
          element={<HomePage />}
        />
        <Route
          path="/contact-us"
          element={<ContactUsPage />}
        />
        <Route
          path="/about-us"
          element={<AboutUsPage />}
        />
        <Route
          path="/faqs"
          element={<FAQsPage />}
        />
        <Route
          path="/catalog"
          element={<CatalogPage />}
        />
        <Route
          path="*"
          element={<NotFoundPage />}
        />
      </Routes>
    </div>
  );
};

export default App;
