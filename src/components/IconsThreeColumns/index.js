import './style.scss';

import Heading from '@c/Text/Heading';
import TextElement from '@c/Text/TextElement';

function IconsThreeColumns(props) {
  const { title, items } = props;

  return (
    <div className="r-icons-three-col o-container">
      <Heading
        text={title}
        depth={2}
        pattern="h1"
        alignment="center"
        indent="8"
      />
      <div className="r-icons-three-col__wrapp wrapp">
        {
          items.map((item, index) =>
            <div
              className="wrapp__item item"
              key={ index }
            >
              <img
                src={ item.imageUrl }
                alt="icon"
                className="item__icon"
              />
              <Heading
                text={ item.title }
                depth={3}
                pattern="h2"
                indent="4"
              />
              <TextElement
                size="medium"
                text={item.description}
                indent="none"
              />
            </div>
          )
        }
      </div>
    </div>
  );
};

export default IconsThreeColumns;
