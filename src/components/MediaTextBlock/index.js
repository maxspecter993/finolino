import './style.scss';

import Heading from '@c/Text/Heading';
import TextElement from '@c/Text/TextElement';
import Btn from '@c/Buttons/Btn';

export default function MediaTextBlock(props) {
  const {
    title,
    description,
    btn,
    image,
  } = props;

  return (
    <div className="r-media-text o-container">
      <div className="r-media-text__media">
        {
          (image && image.url)
          &&
          <img
            data-swiper-parallax="-600"
            className="r-hero-slide__img"
            src={image.url}
            alt={image.alt}
          />
        }

      </div>
      <div className="r-media-text__content">
        {
          title &&
          <Heading
            text={title}
            depth={2}
            pattern="h2"
            indent="5"
          />
        }
        {
          description &&
          <TextElement
            text={description}
            size="medium"
            indent="8"
            isHtml
          />
        }

        {
          (btn && btn.text)
          &&
          <Btn
            url={btn.url}
            text={btn.text}
            tag="a"
            size="small"
          />
        }

      </div>
    </div>
  );
};
