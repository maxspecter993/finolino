import { Component } from 'react';
import { Link } from "react-router-dom";
import './style.scss';

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.navItems = [
      {
        title: 'Catalog',
        link: '/catalog',

      },
      {
        title: 'Contact Us',
        link: '/contact-us',

      },
      {
        title: 'About Us',
        link: '/about-us',
      },
      {
        title: 'FAQs',
        link: '/faqs',
      }
    ];

    this.controlItems = [
      {
        icon: 'icon-search',
        title: 'Search',
      },
      {
        icon: 'icon-heart',
        title: 'Favourites',
      },
      {
        icon: 'icon-user',
        title: 'Personal area',
      },
      {
        icon: 'icon-cart',
        title: 'Cart',
      },
    ];
  }
  render() {
    return (
      <div className="r-header">
        <div className="o-header-container r-header__container">
          <ul className="r-header__nav nav">
            { this.navItems.map((item, index) =>
              <li key={index}>
                <Link
                  className="nav__item"
                  to={item.link}
                >
                  {item.title}
                </Link>
              </li>
            ) }
          </ul>
          <ul className="r-header__control control">
            { this.controlItems.map((item, index) =>
              <li key={index}>
                <button
                  type="button"
                  className="control__item"
                  title={item.title}
                >
                  <i className={item.icon} />
                </button>
              </li>
            ) }
          </ul>
          <Link
            className="r-header__logo"
            to="/"
          >
            <i className="icon-logo" />
          </Link>
        </div>
      </div>
    );
  }
};
