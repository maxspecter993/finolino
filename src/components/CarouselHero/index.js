import React from 'react';
import Swiper from 'swiper/bundle';
import { CSSTransition } from 'react-transition-group';
import classNames from 'classnames';
import './style.scss';

import CarouselHeroSlide from '@c/CarouselHeroSlide';

class CarouselHero extends React.Component {
  constructor(props) {
    super(props);
    this.swiperEl = React.createRef();
    this.swiper = undefined;
    this.lightBgColors = ['tur-blue'];
    this.state = {
      activeBgColor: undefined,
      bgColors: (this.props.items || []).map(item => item.bgColor || '').filter((item, index, array) => {
        return array.indexOf(item) === index;
      }),
    };
  }
  componentDidMount() {
    this.swiper = new Swiper(this.swiperEl.current, {
      loop: false,
      parallax: true,
      spaceBetween: 500,
      speed: 1500,
      autoplay: {
        delay: 7000,
      },
    });
    this.onSlideChange(this.swiper);
    this.swiper.on('slideChange', this.onSlideChange);
  }

  render() {
    const { items } = this.props;

    return (
      <div className="r-carousel-hero">
        { this.state.bgColors.map((bgColor) =>
          <CSSTransition
            key={ bgColor }
            in={ bgColor === this.state.activeBgColor }
            timeout={ 1000 }
            classNames="u-opac"
            unmountOnExit
          >
            <div
              className={ classNames(
                'r-carousel-hero__bg',
                { [`r-carousel-hero__bg--${bgColor}`]: bgColor }
              ) }
            />
          </CSSTransition>
        ) }
        <div
          className="swiper-container r-carousel-hero__swiper"
          ref={this.swiperEl}
        >
          <div
            className="r-carousel-hero__parallax"
            data-swiper-parallax="-23%"
          />
          <div className="swiper-wrapper">
            {
              items.map((item, index) =>
                <CarouselHeroSlide
                  className="swiper-slide"
                  key={index}
                  title={item.title}
                  description={item.description}
                  btn={item.btn}
                  textColor={this.getTextColor(item.bgColor)}
                  image={item.image}
                  imagePosition={item.imagePosition}
                />
              )
            }
          </div>
        </div>
      </div>
    );
  }

  onSlideChange = ({ activeIndex }) => {
    const activeBgColor = this.props.items[activeIndex].bgColor || '';
    const bgColors = this.state.bgColors.slice().sort((a, b) => {
      if (a === activeBgColor) return 1;
      if (b === activeBgColor) return -1;
      return 0;
    });
    this.setState({ activeBgColor, bgColors });
  }

  getTextColor = (bgColor) => {
    return this.lightBgColors.includes(bgColor) ? 'indigo' : 'white';
  }
}

export default CarouselHero;
