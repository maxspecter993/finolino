import { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

export default class ImageCovered extends Component {
  render() {
    const {
      className,
      url,
      alt,
      bgPosition = 'center',
    } = this.props;

    return (
      <div
        style={{ backgroundImage: `url(${url})` }}
        className={ classNames(
          className,
          'r-image-covered',
          { [`r-image-covered--pos-${bgPosition}`]: bgPosition }
        ) }
      >
        <img
          src={url}
          alt={alt}
          className="r-image-covered__img"
        />
      </div>
    );
  }
};


ImageCovered.propTypes = {
  bgPosition: PropTypes.oneOf(['center', 'left', 'right', 'top', 'bottom']),
};

