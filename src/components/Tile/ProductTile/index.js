import { Component } from 'react';
import TextElement from '@c/Text/TextElement';
import ImageCovered from '@c/ImageCovered';

import './style.scss';

export default class ProductTile extends Component {
  render() {
    return (
      <div className="r-product-tile">
        <ImageCovered
          className="r-product-tile__img"
          url={this.props.image?.url}
          alt={this.props.image?.alt}
          bgPosition="top"
        />
        <p className="r-product-tile__tag">
          New
        </p>
        <div className="r-product-tile__content">
          <TextElement
            text={this.props.title}
            color="grey-10"
            indent="1"
          />
          <TextElement
            text={this.props.price}
            color="grey-10"
            indent="none"
          />
        </div>
      </div>
    );
  }
};
