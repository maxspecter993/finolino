import React from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default class Btn extends React.Component {
  render() {
    const {
      url,
      text,
      tag = 'a',
      size,
      color = 'white',
      roundingType,
    } = this.props;
    const CustomTag = tag;

    return (
      <CustomTag
        className={classNames(
          'o-btn',
          {
            [`o-btn--rounding-${roundingType}`]: roundingType,
            [`o-btn--size-${size}`]: size,
            [`o-btn--color-${color}`]: color,
          }
        )}
        href={url}
      >
        {text}
      </CustomTag>
    )
  }
}

Btn.propTypes = {
  tag: PropTypes.oneOf(['a', 'button']).isRequired,
  roundingType: PropTypes.oneOf(['top-left', 'top-right', 'bottom-left', 'bottom-right']),
  size: PropTypes.oneOf(['default', 'middle', 'small']),
};
