import { Component } from 'react';
import CarouselAdv from '@c/CarouselAdv';
import CarouselHero from '@c/CarouselHero';
import HalfSelector from '@c/HalfSelector';
import IconsThreeColumns from '@c/IconsThreeColumns';
import MediaTextBlock from '@c/MediaTextBlock';

export default class HomePage extends Component {
  render() {
    return (
      <>
        <CarouselHero
          items={[
            {
              title: 'New collection',
              description: '',
              btn: {
                text: 'Go to catalog',
                url: '/catalog',
              },
              image: {
                url: `${process.env.PUBLIC_URL}/images/banner_hero_01.png`,
                alt: '',
              },
              imagePosition: 'right',
              bgColor: 'fire-bush',
            },
            {
              title: 'Brutal collection',
              description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod',
              btn: {
                text: 'Go to catalog',
                url: '/catalog',
              },
              image: {
                url: `${process.env.PUBLIC_URL}/images/banner_hero_03.png`,
                alt: '',
              },
              imagePosition: 'left',
              bgColor: 'grey-10',
            },
            {
              title: 'Shiva Style',
              description: '',
              btn: {
                text: 'Go to catalog',
                url: '/catalog',
              },
              image: {
                url: `${process.env.PUBLIC_URL}/images/banner_hero_04.png`,
                alt: '',
              },
              imagePosition: 'left',
              bgColor: 'geraldine',
            },
            {
              title: 'Skinny collection',
              description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Purus viverra accumsan in nisl nisi. Ut enim blandit volutpat maecenas volutpat. Mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Fermentum odio eu feugiat pretium nibh ipsum consequat.',
              btn: {
                text: 'Go to catalog',
                url: '/catalog',
              },
              image: {
                url: `${process.env.PUBLIC_URL}/images/banner_hero_05.png`,
                alt: '',
              },
              imagePosition: 'right',
              bgColor: 'tur-blue',
            },
          ]}
        />
        <HalfSelector
          items={[
            {
              title: 'Men',
              imageUrl: `${process.env.PUBLIC_URL}/images/man_selector.png`,
              url: '/men',
            }, {
              title: 'Women',
              imageUrl: `${process.env.PUBLIC_URL}/images/woman_selector.jpg`,
              url: '/women',
            },
          ]}
        />
        <IconsThreeColumns
          title="About Us"
          items={[
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Worldwide shipping',
              description: 'Morbi eu orcielementum nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Timely servise',
              description: 'Morbi eu orcielementum nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Bonuses and discounts',
              description: 'Morbi eu orcielementum nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Quality assurance',
              description: 'Morbi eu orcielementum nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Making the whole image',
              description: 'Morbi eu orcielementum vitae orci ante nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Exchange and return',
              description: 'Morbi eu orcielementum nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
            {
              imageUrl: `${process.env.PUBLIC_URL}/images/i-shipping.svg`,
              title: 'Exchange and return',
              description: 'Morbi eu orcielementum nisl fermentum faucibus. Vivamus vitae orci ante.',
            },
          ]}
        />
        <CarouselAdv
          items={[
            {
              title: 'Lorem ipsum dolor sit amet',
              description: '1000 UAH and under',
              image: {
                url: `${process.env.PUBLIC_URL}/images/bnr_01.jpg`,
                alt: '',
              },
              url: '#',
            },
            {
              title: 'Lorem ipsum dolor sit amet',
              description: '1000 UAH and under',
              image: {
                url: `${process.env.PUBLIC_URL}/images/bnr_02.jpg`,
                alt: '',
              },
              url: '#',
            },
            {
              title: 'Lorem ipsum dolor sit amet',
              description: '1000 UAH and under',
              image: {
                url: `${process.env.PUBLIC_URL}/images/bnr_03.jpg`,
                alt: '',
              },
              url: '#',
            },
            {
              title: 'Lorem ipsum dolor sit amet',
              description: '1000 UAH and under',
              image: {
                url: `${process.env.PUBLIC_URL}/images/bnr_04.jpg`,
                alt: '',
              },
              url: '#',
            },
          ]}
        />
        <MediaTextBlock
          title="Our story"
          description={`Aenean sagittis, tellus tempor hendrerit aliquam, dui augue varius quam, sit amet facilisis nibh est quis enim.
          Quisque mi leo, fermentum in erat gravida, aliquet blandit ligula. Nunc sed enim id sapien molestie ornare. Nunc quis mollis
          nibh.<br /><br />
          Cras vel elit rhoncus, maximus tellus tempor, imperdiet purus. Proin in ex ut turpis tempus volutpat.Vestibulum sapien turpis,
          commodo vitae fermentum in, consectetur ac nisi.`}
          btn={{
            text: 'Learn more',
            url: '#',
          }}
          image={{
            url: `${process.env.PUBLIC_URL}/images/story-img.jpg`,
            alt: 'Story Image',
          }}
        />
      </>
    );
  }
};
