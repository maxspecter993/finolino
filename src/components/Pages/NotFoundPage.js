import { Component } from 'react';

export default class NotFoundPage extends Component {
  render() {
    return (
      <div>
        <h1>404 not Found</h1>
      </div>
    );
  }
};
