import { Component } from 'react';
import GridMain from '@c/Grid/GridMain';
import ProductTile from '@c/Tile/ProductTile';

import axios from 'axios';

export default class CatalogPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      error: null,
    };
  }

  getThumbnailFromGallery(gallery) {
    const list = gallery.data;
    if (!(list && list.length)) return '';

    const imgObj = list[0];
    const relativeUrl = imgObj.attributes?.formats?.small?.url;
    const alt = imgObj.attributes?.alternativeText;
    return {
      url: `http://localhost:1337${relativeUrl}`,
      alt: alt,
    };
  }

  mapProductsResponse(data = {}) {
    return data.data.map((item) => {
      const price = item.attributes?.price;
      const priceStr = price ? `${price} UAH` : '';
      return {
        id: item.id,
        ...item.attributes,
        price: priceStr,
        thumbnail: this.getThumbnailFromGallery(item.attributes?.images),
      }
    });
  }

  // Fetch your restaurants immediately after the component is mounted
  componentDidMount = async () => {
    console.log('blaaaa');
    try {
      const response = await axios.request({
        url: 'http://localhost:1337/api/products/?populate=Avatar,images,categories',
        method: 'get',
      });
      this.setState({ products: this.mapProductsResponse(response.data) });
    } catch (error) {
      this.setState({ error });
    }
  };

  render() {
    const { products, error } = this.state;
    console.log('products', products);
    if (error) {
      return <div>An error occured: {error.message}</div>;
    }

    const list = products.map(item => (
      <ProductTile
        key={item.id}
        image={item.thumbnail}
        title={item.title}
        price={item.price}
      />
    ));

    return (
      <div className="u-bg-color-grey-88">
        <div className="o-container">
          <GridMain slot={list} />
        </div>
      </div>
    );
  }
};
