import DOMPurify from 'dompurify';

export function getClearHtml(myHtml) {
  return DOMPurify.sanitize(myHtml);
}

export function safelySetInnerHTML(myHtml) {
  return { __html: getClearHtml(myHtml) };
}
