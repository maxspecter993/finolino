const path = require("path");
const sassResourcesLoader = require('craco-sass-resources-loader');

module.exports = {
  webpack: {
    alias: {
      '@': path.resolve(__dirname, "src/"),
      '@c': path.resolve(__dirname, "src/components/"),
      '@root': path.resolve(__dirname),
    },
  },
  plugins: [
    {
      plugin: sassResourcesLoader,
      options: {
        resources: './src/assets/scss/settings-tools.scss',
      },
    },
  ],
};
